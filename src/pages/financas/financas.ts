import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Financa } from '../../models/financa';
import { AdicionaFinancaPage } from '../adiciona-financa/adiciona-financa';
import { EditaFinancaPage } from '../edita-financa/edita-financa';
import { FinancaServiceProvider } from '../../providers/financa-service/financa-service';
import { AdicionaMetaPage } from '../adiciona-meta/adiciona-meta';
import { MetaGasto } from '../../models/meta';
import { MetaServiceProvider } from '../../providers/meta-service/meta-service';
import { EditaMetaPage } from '../edita-meta/edita-meta';


@IonicPage()
@Component({
  selector: 'page-financas',
  templateUrl: 'financas.html',
})
export class FinancasPage {

  public financas: Financa[] = [];
  public metas: MetaGasto[] = [];

  // Mês Atual
  public hoje = new Date();
  public mesAtual = this.hoje.getMonth() + 1;

  // Mostrar financas mensal ou geral
  public selecionouFinancasGerais = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams, 
    public financaService: FinancaServiceProvider,
    public metaService: MetaServiceProvider,
    public alertCtrl: AlertController) {

    }

  ionViewWillEnter() {
    this.carregaFinancasFB();
    this.carregaMetasFB();
  }

  carregaFinancasFB() {
    this.financaService.recebeFinancasFB()
      .then(financas => {
        this.financas = this.ordenaFinancas(this.filtraFinancas((financas as Financa[])));
    });
  }

  carregaMetasFB() {
    this.metaService.recebeMetasFB()
      .then(metas => {
        this.metas = metas as MetaGasto[] || [];
    });
  }

  modalAdicionaFinanca() {
    this.navCtrl.push(AdicionaFinancaPage);
  }

  modalAdicionaMeta() {
    this.navCtrl.push(AdicionaMetaPage);
  }

  modalEditaFinanca(financa: Financa) {
    this.navCtrl.push(EditaFinancaPage, financa);
  }

  modalEditaMeta(meta: MetaGasto) {
    this.navCtrl.push(EditaMetaPage, meta);
  }

  removeFinanca(financa: Financa) {
    let alert = this.alertCtrl.create({
      title: "Excluir finança",
      message: "Você tem certeza que deseja excluir esta finança?"
        + "\n\n" + "Todas as informações serão deletadas.",
      buttons: [{
        text: 'Cancelar',
        handler: () => {}
      },
      {
        text: 'Excluir',
        handler: () => {
          this.financaService.removeFinancaFB(financa)
          .then(_ => {
            this.financas = this.financas.filter(fin => fin.key !== financa.key);
            this.metaService.debitaFinanca(financa);
          })
        }
      }

      ]
    });
    alert.present();
  }

  removeMeta(meta: MetaGasto) {
    let alert = this.alertCtrl.create({
      title: "Excluir meta",
      message: "Você tem certeza que deseja excluir esta meta?"
        + "\n\n" + "Todas as informações serão deletadas.",
      buttons: [{
        text: 'Cancelar',
        handler: () => {}
      },
      {
        text: 'Excluir',
        handler: () => {
          this.metaService.removeMetaFB(meta)
          .then(_ => {
            this.metas = this.metas
              .filter(met => met.key !== meta.key);
          })
        }
      }

      ]
    });
    alert.present();
  }

  existeFinanca() {
    return this.financas.length > 0;
  }

  existeMeta() {
    return this.metas.length > 0;
  }

  public retornaSomaCredito(lista) {
    var soma = 0.0;
    for(var i = 0; i < lista.length; i++) {
      if (lista[i].ehDebito == false) {
        soma += +lista[i].valor;
      }
    }
    return this.formataValor(soma);
  }

  public retornaSomaDebito(lista) {
    var soma = 0.0;
    for(var i = 0; i < lista.length; i++) {
      if (lista[i].ehDebito == true) {
        soma += +lista[i].valor;
      }
    }
    return this.formataValor(soma);
  }

  formataValor(valor: number) {
    return Intl.NumberFormat("pt-BR", { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(valor);
  }

  public retornaCategoria(cat: string) {
    const categoria = {
      "alimentacao": "Alimentação",
      "vestuario":"Vestuário",
      "entretenimento": "Entretenimento",
      "bebida": "Bebida",
      "supermercado": "Supermercado",
      "transporte": "Transporte",
      "eletronicos": "Eletrônicos",
      "outros": "Outros"
    }
    return categoria[cat];
  }

  getTextoData(financa: Financa) {
    var options = { weekday: "long", month: 'long', day: '2-digit' };
    let dataSeparada = financa.data.toString().split("-");
    let ano = Number(dataSeparada[0]);
    let mes = Number(dataSeparada[1]) - 1;
    let dia = Number(dataSeparada[2]);
    let dataAjustada = new Date(ano, mes, dia);
    return dataAjustada.toLocaleDateString("pt-br", options);
  }

  filtraFinancas(financas: Financa[]) {
    if (this.selecionouFinancasGerais) {
      return financas;
    }
    else {
      var financasFiltradas: Financa[] = [];

      for (var i = 0; i < financas.length; i++) {
        var dataFinancaAtual = new Date(financas[i].data);
        if (dataFinancaAtual.getMonth() == this.mesAtual - 1) {
          financasFiltradas.push(financas[i]);
        }
      }
      return financasFiltradas;
    }
  }

  financasGerais(resposta) {
    if (resposta == "sim") {
      this.selecionouFinancasGerais = true;
      this.ionViewWillEnter();
    }
    else {
      this.selecionouFinancasGerais = false;
      this.ionViewWillEnter();
    }
  }

  public retornaNomeMes() {
    switch (this.mesAtual) {
      case 1: return "janeiro";
      case 2: return "fevereiro";
      case 3: return "março";
      case 4: return "abril";
      case 5: return "maio";
      case 6: return "junho";
      case 7: return "julho";
      case 8: return "agosto";
      case 9: return "setembro";
      case 10: return "outubro";
      case 11: return "novembro";
      default: return "dezembro";
    }
  }

  public retornaPorcentagemMeta(meta: MetaGasto) {
    return this.metaService.calcProgessoMeta(meta);
  }

  public retornaTotalMeta(meta: MetaGasto) {
    return this.metaService.calcTotalMeta(meta);
  }

  private formataDinheiro(n) {
    return "R$ " + n.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
    }

  private ordenaFinancas(financas: Financa[]) : Financa[] {
    return financas.sort(this.compareFinancas);
  }

  private compareFinancas(a: Financa, b: Financa) {
    var dataA = new Date(a.data);
    var dataB = new Date(b.data);
    if (dataA.getTime() > dataB.getTime()) {
      return -1;
    }
    if (dataA.getTime() < dataB.getTime()) {
      return 1;
    }
    return 0; 
  }


}
