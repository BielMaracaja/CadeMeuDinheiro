import { MetaGasto } from "./meta";

export class Usuario {
    nome: string;
    username: string;
    profissao: string;
    salario: number;
    email: string;
    fotosrc: string;
    userId: string;
    pushToken: string;
    metas: MetaGasto[];
}

