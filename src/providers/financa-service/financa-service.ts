import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject, AngularFireList } from 'angularfire2/database';
import { AuthProvider } from '../auth/auth';
import { Financa } from '../../models/financa';


@Injectable()
export class FinancaServiceProvider {

  private financas: Financa[] = [];

  constructor(private db: AngularFireDatabase,
    private auth: AuthProvider) {
  }


  public adicionaFinancaFB(financa: Financa) {
    this.formataValorFB(financa);
    return new Promise((resolve) => {
      this.getFinancaListRef().push(financa)
        .then(resp => {
          const novaFinanca = this.setFinancaKey(financa, resp.key);
          resolve(novaFinanca);
      });
    });
  }

  public adicionaFinancaEmUsuarioFB(username: string, financa: Financa) {
    this.formataValorFB(financa);
    return new Promise((resolve) => {
      this.getFinancaListRefParam(username).then(resp => {
        var financaList = resp as string;
        this.db.list(financaList)
        .push(financa)
        .then(resp => {
          const novaFinanca = this.setFinancaKey(financa, resp.key);
          console.log("compartilhado com sucesso!!!")
          resolve(novaFinanca);
        })
      });
    });
  }

  public editaFinancaFB(financa: Financa) {
    this.formataValorFB(financa);
    return this.getFinancaListRef().update(financa.key, financa);
  }

  public removeFinancaFB(financa: Financa) {
    return this.getFinancaListRef().remove(financa.key);
  }

  public recebeFinancasFB() {
    return new Promise((resolve) => {
      this.getFinancaObjRef()
      .valueChanges()
      .subscribe(financas => {
        if(financas) {
          this.financas = this.mapFinObjectToList(financas);
        }
        resolve(this.financas);
      });
    });
  }

   private getFinancaListRef() {
    return this.db.list(this.getFinancaPath());
  }

  private getFinancaObjRef() {
    return this.db.object(this.getFinancaPath());
  }

  private getFinancaPath() {
    return "financa-list/" + this.auth.getUID();
  }

  private getFinancaListRefParam(username: string) {
    return new Promise((resolve, reject) => {
      this.auth.getUsuarioKey(username).then(key => {
        resolve("financa-list/" + key);
      });
    });
  }

  private mapFinObjectToList(financaObj:{}) {
    return Object.keys(financaObj)
      .map(key => {
        let financa = financaObj[key];
        return this.setFinancaKey(financa, key);
      });
  }

  private setFinancaKey(financa: {}, key: string) {
    return {
      ...financa,
      key: key
    } as Financa;
  }

  private formataValorFB(financa: Financa) {
    let valor = String(financa.valor);
    while (valor.includes(".")) {
      valor = valor.replace(".","");
    }
    financa.valor = Number(valor.replace(",","."));
  }
}
